FROM gradle:4.9.0-jdk10-slim as build

WORKDIR /app
COPY build.gradle .
COPY gradlew .
COPY gradlew.bat .
COPY settings.gradle .
COPY src src

RUN ./gradlew build

FROM openjdk:10.0.1-10-jre-slim

WORKDIR /app
EXPOSE 8080
ENV STORE_ENABLED=true
ENV WORKER_ENABLED=true
COPY --from=build /lib/orderEntity-tracking-0.0.1SNAPSHOT.jar /app

CMD ["java", "-jar", "orderEntity-tracking-0.0.1SNAPSHOT.jar"]
