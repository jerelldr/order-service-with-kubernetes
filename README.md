# Order Service POC

This project attempts to demonstrate design choices for a fictitious order system. The focus was to make it scalable, testable, extensible and cloud/container friendly.  There is still a ton of work to accomplish the goal, but I believe the spirit of the design is evident.

This project uses features introduced in Java 10. Mostly, we chose to use JDK 10 because it brings with it some syntactic niceties, as well as some container friendly improvements. See below article for a brief synopsis.

[Docker Support in Java 10](https://medium.com/@jnsrikanth/docker-support-in-java-10-fbff28a31827) 

The project uses liquibase for database migrations/changeset tracking.  Database versioning should be just as crucial to a projects success as src code versioning. We should be able to "travel" forwards and backwards through versions and our database and src code should remain in sync. Liquibase gives that capability with the concept of versioned updates and rollbacks.

[Liquibase Updates](https://www.liquibase.org/documentation/update.html)


[Liquibase Rollbacks](https://www.liquibase.org/documentation/update.html)


## Getting Started

`git clone git@gitlab.com:jerelldr/order-service-with-kubernetes.git`

`./gradlew run` 

Or

Use your favourite IDE to run it.


### Prerequisites

* JDK 10
* Lombock (will be integrated when it's fully supported in JDK 10)
* Docker (optional)
* minikube (optional)

```
Give examples
```

### Installing (TODO)

Local Kubernetes Instance Instructions

Minikube

* Project URL: https://github.com/kubernetes/minikube
    Latest Release and download instructions: https://github.com/kubernetes/minikube/releases
* VirtualBox: http://www.virtualbox.org

Minikube on windows:

Download the latest minikube-version.exe

Rename the file to minikube.exe and put it in C:\minikube

Open a cmd (search for the app cmd or powershell)

Run: `cd C:\minikube and enter minikube start`

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests (TODO)

Explain how to run the automated tests for this system

### Break down into end to end tests (TODO)

Explain what these tests test and why

```
Give an example
```

### And coding style tests (TODO)

Explain what these tests test and why

```
Give an example
```

## Deployment

Deployable with Docker/Kubernetes:

Containers:
* View Layer 
* ActiveMQ Messaging Layer
* Service Layer

## Local Kubernetes Intructions




## Built With

* [Spring Mvc](https://docs.spring.io/spring/docs/current/spring-framework-reference/web.html) - The web framework used
* [Thymeleaf](https://www.thymeleaf.org/) Server side templating engine
* [Gradle](https://gradle.org/) - Build tool
* [Mockito](http://site.mockito.org/) - Mock framework for unit tests
* [Lombock](https://projectlombok.org/) - Annotation based framework to reduce verbosity of language (has issues with Java 10)
* [Gatling](https://gatling.io/performancetesting/) - Integration Testing - only on develop branch
* [Liquibase](https://www.liquibase.org/documentation/index.html) - Database refactoring/migrations

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/jerelldr/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/jerelldr/order-service-with-kubernetes/tags). 

## Authors

* **Jerell Robinson** - *Initial work* - [JerellDR](https://gitlab.com/jerelldr)
See also the list of [contributors](https://gitlab.com/jerelldr/order-service-with-kubernetes/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [https://allylearning.udemy.com/learn-devops-the-complete-kubernetes-course]() - install instructions for local kubernetes
* Inspiration
* etc
