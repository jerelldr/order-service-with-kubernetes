package com.order.tracking;

import com.order.tracking.config.SecurityConfig;
import com.order.tracking.services.queue.QueueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.jms.annotation.JmsListenerConfigurer;
import org.springframework.jms.config.JmsListenerEndpointRegistrar;
import org.springframework.jms.config.SimpleJmsListenerEndpoint;

@SpringBootApplication
@Import({SecurityConfig.class })
public class OrderTrackingApplication implements JmsListenerConfigurer {

  @Value("${queue.name}")
  private String queueName;

  @Value("${worker.name}")
  private String workerName;

  @Value("${worker.enabled}")
  private boolean workerEnabled;

  @Autowired
  private QueueService queueService;


  public static void main(String[] args) {
    SpringApplication.run(OrderTrackingApplication.class, args);
  }

  @Override
  public void configureJmsListeners(JmsListenerEndpointRegistrar registrar) {
    registerQueueListener(registrar);
  }

  private void registerQueueListener(JmsListenerEndpointRegistrar registrar) {
    if (workerEnabled) {
      SimpleJmsListenerEndpoint endpoint = new SimpleJmsListenerEndpoint();
      endpoint.setId(workerName);
      endpoint.setDestination(queueName);
      endpoint.setMessageListener(queueService);
      registrar.registerEndpoint(endpoint);
    }
  }
}
