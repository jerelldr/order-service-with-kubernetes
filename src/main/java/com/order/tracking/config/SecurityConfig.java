package com.order.tracking.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;


@EnableJms
@EntityScan("com.order.tracking.entity")
@ComponentScan
//@EnableJpaRepositories("com.order.tracking.repository")
@Configuration
public class SecurityConfig {

}
