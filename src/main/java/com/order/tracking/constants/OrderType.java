package com.order.tracking.constants;

public enum OrderType {
  PREMIUM  {
    @Override
    public String toString() {
      return "PREMIUM";
    }
  },
  BASIC  {
    @Override
    public String toString() {
      return "BASIC";
    }
  }
}
