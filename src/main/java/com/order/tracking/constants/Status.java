package com.order.tracking.constants;

public enum Status {
  PENDING {
    @Override
    public String toString() {
      return "PENDING";
    }
  },
  PROCESSED {
    @Override
    public String toString() {
      return "PROCESSED";
    }
  },
  PAYED {
    @Override
    public String toString() {
      return "PAYED";
    }
  }, SHIPPED {
    @Override
    public String toString() {
      return "SHIPPED";
    }
  }
}
