package com.order.tracking.controller;

import com.order.tracking.constants.Status;
import com.order.tracking.entity.Sku.Premium;
import com.order.tracking.entity.Sku.Sku;
import com.order.tracking.entity.order.OrderEntity;
import com.order.tracking.exceptions.OverPaymentException;
import com.order.tracking.model.order.OrderTracking;
import com.order.tracking.model.order.Payment;
import com.order.tracking.services.order.OrderService;
import com.order.tracking.services.queue.QueueService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OrderController {

  private static final Logger LOGGER = LoggerFactory.getLogger(OrderController.class);

  @Autowired
  private QueueService queueService;

  @Autowired
  private OrderService orderService;

  @Value("${queue.name}")
  private String queueName;

  @Value("${worker.name}")
  private String workerName;

  @Value("${store.enabled}")
  private String storeEnabled;

  @Value("${worker.enabled}")
  private String workerEnabled;

  @GetMapping("/")
  public String home(Model model) {
    int pendingMessages = queueService.pendingJobs(queueName);
    initModal(model, pendingMessages);
    return "home";
  }

  @PostMapping("/order")
  public String order(@ModelAttribute Sku sku) {
    LOGGER.info("/order", sku);

    orderService.create(sku);

    return "success";
  }

  @GetMapping("/payment")
  public String payments(Model model) {
    LOGGER.info("payment");

    List<OrderEntity> orderEntities = orderService.findByStatus(Status.PROCESSED);

    var Payment = new Payment();
    model.addAttribute("orderEntities", orderEntities);
    model.addAttribute("payment", Payment);
    model.addAttribute("isStoreEnabled", this.storeEnabled);

    return "payments";
  }

  @GetMapping("/order/{orderNumber}")
  public ResponseEntity order(@PathVariable String orderNumber, Model model) {
    LOGGER.info("orderNumber", orderNumber);

    OrderEntity orderEntity = orderService.findByOrderNumber(orderNumber);

    return new ResponseEntity(orderEntity, HttpStatus.OK);
  }

  @GetMapping("/orders")
  public ResponseEntity orders() {
    LOGGER.info("retrieving processed and unprocessed orders");

    OrderTracking orderTracking = orderService.retrieveOrderTrackingInformation();

    return new ResponseEntity(orderTracking, HttpStatus.OK);
  }

  @PostMapping("/pay")
  public String payment(@ModelAttribute Payment payment) {
    LOGGER.info("preparing order for payment.");

    try {
      orderService.processPayment(payment);
    } catch (OverPaymentException e) {
      LOGGER.error("payments", e);
      return "payment-error";
    } catch (Exception e) {
      LOGGER.error("payments", e);
      return "error";
    }

    return "payment-success";
  }

  @ResponseBody
  @RequestMapping(value = "/metrics", produces = "text/plain")
  public String metrics() {
    LOGGER.info("Retrieving metrics...");

    String metrics = queueService.retrieveMetrics();

    return metrics;
  }

  @RequestMapping(value = "/health")
  public ResponseEntity health() {
    HttpStatus status = queueService.retrieveServerStatus();

    return new ResponseEntity<>(status);
  }

  private void initModal(Model model, int pendingMessages) {
    model.addAttribute("sku", new Premium());
    model.addAttribute("pendingJobs", pendingMessages);
    model.addAttribute("completedJobs", queueService.completedJobs());
    model.addAttribute("isConnected", queueService.isUp() ? "Yes" : "No");
    model.addAttribute("queueName", this.queueName);
    model.addAttribute("workerName", this.workerName);
    model.addAttribute("isStoreEnabled", this.storeEnabled);
    model.addAttribute("isWorkerEnabled", this.workerEnabled);
  }

}
