package com.order.tracking.controller;

import com.order.tracking.entity.order.OrderEntity;
import com.order.tracking.exceptions.OverPaymentException;
import com.order.tracking.model.order.OrderTracking;
import com.order.tracking.model.order.Payment;
import com.order.tracking.services.order.OrderService;
import com.order.tracking.services.queue.QueueService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ideally all calls to the rest controller would be routed through the queue service
 * this would allow high throughout and afford system scalability
 */
@RestController
@RequestMapping("api/rest/")
public class RestOrderController {

  private static final Logger LOGGER = LoggerFactory.getLogger(RestOrderController.class);


  @Autowired
  private QueueService queueService;

  @Autowired
  private OrderService orderService;

  @Value("${queue.name}")
  private String queueName;

  @Value("${worker.name}")
  private String workerName;

  @Value("${store.enabled}")
  private String storeEnabled;

  @Value("${worker.enabled}")
  private String workerEnabled;


  @PostMapping("/order")
  ResponseEntity<?> createOrder(@RequestBody OrderEntity input) {
    LOGGER.info("Creating order...");

    var entity = orderService.save(input);

    return ResponseEntity.ok(entity);
  }

  @GetMapping("/order/{orderNumber}")
  public ResponseEntity<?> getOrder(@PathVariable String orderNumber) {
    LOGGER.info("Retrieving order with order number", orderNumber);

    var entity = orderService.findByOrderNumber(orderNumber);

    return ResponseEntity.ok(entity);
  }

  @PostMapping("/payment")
  ResponseEntity<?> payment(@RequestBody Payment input) {
    LOGGER.info("preparing order for payment.");

    OrderEntity entity;

    try {
      entity = orderService.processPayment(input);
    } catch(OverPaymentException e) {
      return new ResponseEntity<>(e.getError(), HttpStatus.BAD_REQUEST);
    }

    return ResponseEntity.ok(entity);
  }

  @GetMapping("/orders")
  public ResponseEntity<OrderTracking> orders() {
    LOGGER.info("Retrieving processed and unprocessed orders");

    OrderTracking orderTracking = orderService.retrieveOrderTrackingInformation();

    return ResponseEntity.ok(orderTracking);
  }

}
