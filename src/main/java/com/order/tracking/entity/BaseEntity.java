package com.order.tracking.entity;

import com.order.tracking.constants.Status;

/**
 * all entities should ideally extend from this base class.
 * common attributes would go here
 */
public class BaseEntity {

  private String orderNumber;
  private Status status = Status.PENDING;

  public String getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }
}
