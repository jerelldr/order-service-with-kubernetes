package com.order.tracking.entity.Sku;

import com.order.tracking.constants.OrderType;
import javax.persistence.Embeddable;

@Embeddable
public class Basic extends Sku {
  private final OrderType sku;

  public Basic() {
    sku = OrderType.BASIC;
  }

}
