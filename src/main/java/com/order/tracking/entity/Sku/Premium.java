package com.order.tracking.entity.Sku;

import com.order.tracking.constants.OrderType;
import javax.persistence.Embeddable;

@Embeddable
public class Premium extends Sku {
  private final OrderType sku;

  public Premium() {
    sku = OrderType.PREMIUM;
  }

}
