package com.order.tracking.entity.Sku;

import com.order.tracking.constants.OrderType;
import javax.persistence.Embeddable;


@Embeddable
public class Sku {

  private OrderType skuId = OrderType.BASIC;
  private Integer quantity;
  private Double price;

  public Sku() {

  }

  public Sku(int quantity, double price) {
    this.price = price;
    this.quantity = quantity;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public Integer getQuantity() {
    return quantity;
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public OrderType getSkuId() {
    return skuId;
  }

  public void setSkuId(OrderType skuId) {
    this.skuId = skuId;
  }
}
