package com.order.tracking.entity.order;

import com.order.tracking.constants.Status;
import com.order.tracking.entity.BaseEntity;
import com.order.tracking.entity.Sku.Sku;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "PRODUCT")
public class OrderEntity extends BaseEntity {

  @Id
  @Column(name = "order_number")
  @GeneratedValue(generator = "system-uuid")
  @GenericGenerator(name = "system-uuid", strategy = "uuid2")
  private String orderNumber;

  @Column(name = "quantity")
  private Integer quantity;

  @ElementCollection
  private List<Sku> orders = new ArrayList<>();

  @Column(name = "status")
  @Enumerated(EnumType.ORDINAL)
  private Status status;

  public OrderEntity(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public OrderEntity() {
  }

  public String getTotal() {
    var total = this.orders.stream().filter(sku -> sku.getPrice() > 0.0).mapToDouble(Sku::getPrice)
        .sum();

    return String.valueOf(total);
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public Integer getQuantity() {
    return this.quantity;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public String getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public List<Sku> getOrders() {
    return orders;
  }

  public void OrderEntity(List<Sku> orders) {
    this.orders = orders;
  }

  @Override
  public String toString() {
    return String
        .format("OrderEntity{orderNumber=%s, total=%s}", this.orderNumber, this.getTotal());
  }

}
