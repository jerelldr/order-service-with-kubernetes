package com.order.tracking.exceptions;

public class ErrorInfo {

  private String errorMessage;
  public ErrorInfo(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }
}
