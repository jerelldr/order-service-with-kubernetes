package com.order.tracking.exceptions;

public class OverPaymentException extends RuntimeException {

  private ErrorInfo error;

  public ErrorInfo getError() {
    return this.error;
  }

  public OverPaymentException(ErrorInfo error) {
    this.error = error;
  }

  public OverPaymentException(String message, ErrorInfo errors) {
    super(message);
    this.error = errors;
  }

}
