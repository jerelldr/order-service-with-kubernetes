package com.order.tracking.model;

import com.order.tracking.constants.Status;

public class BaseModel {

  private String orderNumber;
  private Status status = Status.PENDING;

  public String getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }
}
