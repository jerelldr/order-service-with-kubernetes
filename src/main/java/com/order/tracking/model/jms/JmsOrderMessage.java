package com.order.tracking.model.jms;

import com.order.tracking.constants.OrderType;
import com.order.tracking.model.BaseModel;
import java.io.Serializable;

public class JmsOrderMessage  extends BaseModel implements Serializable{
     public String orderNumber;
     public OrderType orderType;
     public int quantity;
     public double price;

     public JmsOrderMessage() {
          //Empty constructor needed for objectMapper
     }
     public JmsOrderMessage(String orderNumber, OrderType orderType, int quantity, double price){
          this.orderNumber = orderNumber;
          this.orderType = orderType;
          this.quantity = quantity;
          this.price = price;
     }

     @Override
     public String toString() {
          return String.format("JmsOrderMessage{orderNumber=%s, orderType=%s, quantity=%s}", this.orderNumber, this.orderType, this.quantity);
     }

}
