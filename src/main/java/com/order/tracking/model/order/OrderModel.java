package com.order.tracking.model.order;

import com.order.tracking.constants.Status;
import com.order.tracking.entity.Sku.Sku;
import java.util.ArrayList;
import java.util.List;


public class OrderModel extends com.order.tracking.model.BaseModel {

  private String orderNumber;
  private Integer quantity;
  private List<Sku> orders = new ArrayList<>();
  private Status status;


  public OrderModel(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public OrderModel() {
  }

  public String getTotal() {
    var total = this.orders.stream().filter(sku -> sku.getPrice() > 0.0).mapToDouble(Sku::getPrice)
        .sum();

    return String.valueOf(total);
  }

  public void setQuantity(Integer quantity) {
    this.quantity = quantity;
  }

  public Integer getQuantity() {
    return this.quantity;
  }

  public Status getStatus() {
    return status;
  }

  public void setStatus(Status status) {
    this.status = status;
  }

  public String getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public List<Sku> getOrders() {
    return orders;
  }

  public void setOrders(List<Sku> orders) {
    this.orders = orders;
  }

  @Override
  public String toString() {
    return String
        .format("JmsOrderMessage{orderNumber=%s, total=%s}", this.orderNumber, this.getTotal());
  }

}
