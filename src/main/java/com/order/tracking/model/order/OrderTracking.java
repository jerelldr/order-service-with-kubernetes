package com.order.tracking.model.order;

public class OrderTracking {

  private Integer numberWaitingForPayment;
  private Integer numberShipped;


  public OrderTracking(int numberWaitingForPayment, int numberShipped){
    this.numberShipped = numberShipped;
    this.numberWaitingForPayment  = numberWaitingForPayment;
  }
  public OrderTracking() {
    // empty constructor
  }

  public Integer getNumberWaitingForPayment() {
    return numberWaitingForPayment;
  }

  public void setNumberWaitingForPayment(Integer numberWaitingForPayment) {
    this.numberWaitingForPayment = numberWaitingForPayment;
  }

  public Integer getNumberShipped() {
    return numberShipped;
  }

  public void setNumberShipped(Integer numberShipped) {
    this.numberShipped = numberShipped;
  }
}
