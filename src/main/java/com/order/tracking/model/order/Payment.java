package com.order.tracking.model.order;

public class Payment {

  private String orderNumber;
  private Double payment;

  public Payment(String orderNumber, double payment) {
    this.orderNumber = orderNumber;
    this.payment = payment;
  }

  public Payment() {

  }



  public String getOrderNumber() {
    return orderNumber;
  }

  public void setOrderNumber(String orderNumber) {
    this.orderNumber = orderNumber;
  }

  public Double getPayment() {
    return payment;
  }

  public void setPayment(Double payment) {
    this.payment = payment;
  }

}
