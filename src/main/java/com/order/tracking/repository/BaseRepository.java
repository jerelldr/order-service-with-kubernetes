package com.order.tracking.repository;

import com.order.tracking.constants.Status;
import com.order.tracking.entity.order.OrderEntity;
import java.util.List;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface BaseRepository<T extends OrderEntity> extends CrudRepository<T, String> {

  T save(T entity);

  T findByOrderNumber(String orderNumber);

  List<T> findAll();

  @Transactional
  @Modifying
  @Query("UPDATE OrderEntity t set t.status = :status where t.orderNumber = :orderNumber")
  void updateStatus(@Param("orderNumber")String orderNumber, @Param("status") Status status);

  @Query("SELECT t FROM OrderEntity t where t.status = :status")
  List<T> findByStatus(@Param("status") Status status);

}
