package com.order.tracking.services.mappers;

import com.order.tracking.entity.BaseEntity;
import com.order.tracking.model.BaseModel;

public class BaseMapper<E extends BaseEntity, M extends BaseModel> {

  public M fromEntityToModel(E entity, M model) {

    model.setOrderNumber(entity.getOrderNumber());
    model.setStatus(entity.getStatus());

    return model;
  }
  public E fromModelToEntity(M model, E entity) {

    entity.setOrderNumber(model.getOrderNumber());
    entity.setStatus(model.getStatus());

    return entity;
  }
}

