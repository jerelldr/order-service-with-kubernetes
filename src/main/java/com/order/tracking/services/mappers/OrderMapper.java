package com.order.tracking.services.mappers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.order.tracking.entity.order.OrderEntity;
import com.order.tracking.model.jms.JmsOrderMessage;
import com.order.tracking.model.order.OrderModel;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class OrderMapper extends BaseMapper<OrderEntity, OrderModel> {

  private ObjectMapper mapper = new ObjectMapper();

  public JmsOrderMessage fromJsonToJmsOrderMessage(String json) throws IOException {
    JmsOrderMessage orderMessage = mapper.readValue(json, JmsOrderMessage.class);
    return orderMessage;
  }

  public OrderEntity fromJmsOrderMessageToProduct(JmsOrderMessage from) {

    OrderEntity orderEntity = new OrderEntity();
    orderEntity.setOrderNumber(from.orderNumber);
    orderEntity.setQuantity(from.quantity);

    return orderEntity;
  }

  public List<OrderModel> fromEntitiesToModels(List<OrderEntity> entities) {
    List<OrderModel> models = new ArrayList<>();

    entities.stream().map(entity -> models.add(fromEntityToModel(entity, new OrderModel())))
        .collect(Collectors.toList());

    return models;
  }

  public List<OrderEntity> fromModelsToEntities(List<OrderModel> models) {
    List<OrderEntity> entities = new ArrayList<>();

    models.stream().map(model -> entities.add(fromModelToEntity(model, new OrderEntity())))
        .collect(Collectors.toList());

    return entities;

  }

}
