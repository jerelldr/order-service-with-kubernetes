package com.order.tracking.services.order;

import com.order.tracking.constants.OrderType;
import com.order.tracking.constants.Status;
import com.order.tracking.exceptions.ErrorInfo;
import com.order.tracking.exceptions.OverPaymentException;
import com.order.tracking.entity.order.OrderEntity;
import com.order.tracking.entity.Sku.Sku;
import com.order.tracking.model.jms.JmsOrderMessage;
import com.order.tracking.model.order.OrderTracking;
import com.order.tracking.model.order.Payment;
import com.order.tracking.repository.OrderRepository;
import com.order.tracking.services.mappers.OrderMapper;
import com.order.tracking.services.queue.QueueService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ideally the controller would send a message to the queueService and the queue service would call into the orderService
 */
@Service
public class OrderService {

  private static final Logger LOGGER = LoggerFactory.getLogger(OrderService.class);

  @Autowired
  private OrderRepository orderRepository;

  @Autowired
  private QueueService queueService;

  @Autowired
  private OrderMapper orderMapper;

  @Value("${queue.name}")
  private String queueName;


  public List<OrderEntity> findAllOrders() {
    try {
      return orderRepository.findAll();
    } catch (Exception e) {
      LOGGER.error("findAll", e);
    }
    return null;
  }

  @Transactional
  public OrderEntity findByOrderNumber(String orderNumber) {
    try {
      return orderRepository.findByOrderNumber(orderNumber);
    } catch (Exception e) {
      LOGGER.error("findByOrderNumber", e);
    }
    return null;
  }

  @Transactional
  public List<OrderEntity> findByStatus(Status status) {
    return orderRepository.findByStatus(status);
  }

  @Transactional
  public OrderEntity save(OrderEntity orderEntity) {
    try {
      orderEntity.setStatus(Status.PENDING);
      return orderRepository.save(orderEntity);
    } catch (Exception e) {
      LOGGER.error("findByStatus", e);
    }
    return null;

  }

  @Transactional
  public OrderEntity saveFromJson(String json) {
    try {
      JmsOrderMessage jmsOrderMessage = orderMapper.fromJsonToJmsOrderMessage(json);
      OrderEntity mappedOrderEntity = orderMapper.fromJmsOrderMessageToProduct(jmsOrderMessage);
      mappedOrderEntity.setStatus(Status.PROCESSED);
      mapJmsOrderOntoOrderEntity(mappedOrderEntity, jmsOrderMessage);
      return orderRepository.save(mappedOrderEntity);
    } catch (Exception e) {
      LOGGER.error("saveFromJson", e);
    }
    return null;

  }

  private void mapJmsOrderOntoOrderEntity(OrderEntity orderEntity,
      JmsOrderMessage jmsOrderMessage) {

    for (int i = 0; i < jmsOrderMessage.quantity; i++) {
      Sku sku = initSku(jmsOrderMessage);
      orderEntity.getOrders().add(sku);
    }
  }

  private Sku initSku(JmsOrderMessage jmsOrderMessage) {
    var sku = new Sku();

    sku.setSkuId(jmsOrderMessage.orderType);
    sku.setPrice(jmsOrderMessage.price);
    sku.setQuantity(jmsOrderMessage.quantity);

    return sku;
  }

  public OrderTracking retrieveOrderTrackingInformation() {

    List<OrderEntity> pendingOrderEntities = findByStatus(Status.PROCESSED);
    List<OrderEntity> processedOrderEntities = findByStatus(Status.SHIPPED);

    OrderTracking orderTracking = initOrderTracking(pendingOrderEntities, processedOrderEntities);

    return orderTracking;
  }

  private OrderTracking initOrderTracking(List<OrderEntity> pendingOrderEntities,
      List<OrderEntity> processedOrderEntities) {

    OrderTracking orderTracking = new OrderTracking();
    orderTracking.setNumberWaitingForPayment(pendingOrderEntities.size());
    orderTracking.setNumberShipped(processedOrderEntities.size());

    return orderTracking;
  }

  public void create(Sku sku) {

    OrderEntity orderEntity = initProduct(sku);
    OrderEntity newModal = save(orderEntity);

    Sku newSku = initSkus(sku, orderEntity);
    JmsOrderMessage jmsMessage = initJmsMessage(newModal, newSku);

    sendToQueue(jmsMessage);
  }

  private JmsOrderMessage initJmsMessage(OrderEntity newModal, Sku newSku) {
    return new JmsOrderMessage(newModal.getOrderNumber(), newSku.getSkuId(),
        newSku.getQuantity(), newSku.getPrice());
  }

  private Sku initSkus(Sku sku, OrderEntity orderEntity) {
    Sku newSku = new Sku();

    newSku.setPrice(sku.getPrice());
    newSku.setQuantity(sku.getQuantity());
    newSku.setSkuId(OrderType.PREMIUM);

    orderEntity.getOrders().add(newSku);

    return newSku;
  }

  private OrderEntity initProduct(Sku sku) {

    OrderEntity orderEntity = new OrderEntity();
    orderEntity.setQuantity(sku.getQuantity());
    orderEntity.setStatus(Status.PENDING);

    return orderEntity;
  }

  public OrderEntity processPayment(Payment payment) {
    var entity = orderRepository.findByOrderNumber(payment.getOrderNumber());

    if (entity == null) {
      return null;
    }

    validatePayment(entity, payment);

    try {

      updateStatus(entity.getOrderNumber(), Status.PAYED);
      updateStatus(entity.getOrderNumber(), Status.SHIPPED);

      LOGGER
          .info(String.format("Shipped Product with product number %s", entity.getOrderNumber()));

      return orderRepository.save(entity);
    } catch (Exception e) {
      LOGGER.error("processPayment", e);
    }

    return null;
  }

  private void validatePayment(OrderEntity entity, Payment payment) {
    if (payment.getPayment() > Double.parseDouble(entity.getTotal())) {
      throw new OverPaymentException(String.format("Over Payment Error: " + payment.getPayment()),
          new ErrorInfo("Over Payment Error: " + payment.getPayment()));
    }
  }

  @Transactional
  @Modifying
  public void updateStatus(String orderNumber, Status status) {
    orderRepository.updateStatus(orderNumber, status);
  }

  private void sendToQueue(JmsOrderMessage message) {
    queueService.send(queueName, message);
  }

}
