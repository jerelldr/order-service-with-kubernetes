package com.order.tracking.services.queue;

import com.order.tracking.model.jms.JmsOrderMessage;
import com.order.tracking.services.order.OrderService;
import java.util.Collections;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class QueueService implements MessageListener {

  private static final Logger LOGGER = LoggerFactory.getLogger(QueueService.class);

  @Autowired
  private JmsTemplate jmsTemplate;

  @Autowired
  private OrderService orderService;

  @Value("${queue.name}")
  private String queueName;

  private int counter = 0;

  private final String PROCESSING_TEMPLATE = "Processing order %s";
  private final String COMPLETED_TEMPLATE = "Completed order %s";
  private final String NOT_A_TEXT_MESSAGE = "Message is not a text message";
  private final String NOT_AN_OBJECT_MESSAGE = "Message is not an object message";

  public int completedJobs() {
    return counter;
  }

  public void send(String destination, String message) {
    LOGGER.info("sending message='{}' to destination='{}'", message, destination);
    jmsTemplate.convertAndSend(destination, message);
  }

  public void send(String destination, JmsOrderMessage message) {
    LOGGER.info("sending message='{}' to destination='{}'", message, destination);
    jmsTemplate.convertAndSend(destination, message);
  }

  public int pendingJobs(String queueName) {
    return jmsTemplate.browse(queueName, (s, qb) -> Collections.list(qb.getEnumeration()).size());
  }

  public boolean isUp() {
    var connection = jmsTemplate.getConnectionFactory();
    try {
      connection.createConnection().close();
      return true;
    } catch (JMSException e) {
      LOGGER.error("isUp", e);
    }
    return false;
  }

  @Override
  public void onMessage(Message message) {
    if (message instanceof ActiveMQTextMessage) {
      ActiveMQTextMessage textMessage = (ActiveMQTextMessage) message;

      try {
        String productJson =  textMessage.getText();
        LOGGER.info(String.format(PROCESSING_TEMPLATE,  textMessage.getText() ));

        orderService.saveFromJson(productJson);

        LOGGER.info(String.format(COMPLETED_TEMPLATE, textMessage));
      } catch (JMSException e) {
        LOGGER.error("onMessage",e);
      }
      counter++;
    }   else {
      LOGGER.error(NOT_AN_OBJECT_MESSAGE, message.toString());
    }
  }

  public String retrieveMetrics() {
    int totalMessages = pendingJobs(queueName);
    return "# HELP messages Number of messages in that queueService\n"
        + "# TYPE messages gauge\n"
        + "messages " + totalMessages;
  }

  public HttpStatus retrieveServerStatus() {
    HttpStatus status;
    if (isUp()) {
      status = HttpStatus.OK;
    } else {
      status = HttpStatus.BAD_REQUEST;
    }
    return status;
  }
}
