package com.order.tracking.config;

import javax.jms.ConnectionFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;

@Configuration
public class JmsConfig {

  @Bean(name="jmsListenerContainerFactory")
  @ConditionalOnProperty(prefix = "spring.activemq", name= "broker-url")
  public JmsListenerContainerFactory<?> jmsListenerContainerFactory(ConnectionFactory activeMQConnectionFactory) {
    DefaultJmsListenerContainerFactory bean = new DefaultJmsListenerContainerFactory();
    bean.setConnectionFactory(activeMQConnectionFactory);
    bean.setPubSubDomain(Boolean.FALSE);
    return bean;
  }


}
