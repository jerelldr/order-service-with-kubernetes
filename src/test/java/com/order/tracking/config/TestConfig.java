package com.order.tracking.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@EntityScan("com.order.tracking.entity")
@ComponentScan
public class TestConfig {

}
