package com.order.tracking.controllers;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.order.tracking.config.JmsConfig;
import com.order.tracking.config.TestConfig;
import com.order.tracking.controller.OrderController;
import com.order.tracking.entity.order.OrderEntity;
import com.order.tracking.entity.Sku.Sku;
import com.order.tracking.model.order.OrderTracking;
import com.order.tracking.repository.OrderRepository;
import com.order.tracking.services.order.OrderService;
import com.order.tracking.services.queue.QueueService;
import java.util.Arrays;
import java.util.List;
import javax.jms.ConnectionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
@Import({JmsConfig.class, TestConfig.class})
@TestPropertySource(locations = "classpath:test.yaml")
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
public class OrderControllerTest {

  @Autowired
  private MockMvc mvc;

  @MockBean
  private OrderService orderService;

  @MockBean
  private QueueService queueService;

  @MockBean
  private OrderRepository orderRepository;

  @Value("${activemq.brokerUrl}")
  private String brokerUrl;

  @MockBean
  @Qualifier("jmsConnectionFactory")
  ConnectionFactory jmsConnectionFactory;


  @Before
  public void setUp() {

    OrderTracking orderTracking = new OrderTracking(1, 1);

    Mockito.when(orderService.retrieveOrderTrackingInformation())
        .thenReturn(orderTracking);
  }

  @Test
  public void givenOrders_whenRetrievePendingAndShippedOrderCount_thenReturnJsonResponse()
      throws Exception {
    OrderEntity orderEntity = new OrderEntity("1234");
    List<OrderEntity> allOrderEntities = Arrays.asList(orderEntity);

    given(orderService.findAllOrders()).willReturn(allOrderEntities);

    MvcResult result = mvc.perform(get("/orders")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    assertThat(result.getResponse().getContentAsString())
        .isEqualTo("{\"numberWaitingForPayment\":1,\"numberShipped\":1}");
  }

  @Test
  public void givenOrder_whenCreate_thenReturnSuccess() throws Exception {
    Sku sku = new Sku();
    doNothing().when(orderService).create(sku);

    MvcResult result = mvc.perform(post("/order")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    assertThat(result.getResponse().getContentAsString()).isNotBlank().isNotNull().isNotEmpty();
  }

  @Test
  public void givenHome_whenPendingJobs_thenReturnHome() throws Exception {
    given(queueService.pendingJobs(Mockito.anyString())).willReturn(3);
    given(queueService.isUp()).willReturn(true);

    MvcResult result = mvc.perform(get("/")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    assertThat(result.getResponse().getContentAsString()).isNotBlank().isNotNull().isNotEmpty();
  }

  @Test
  public void givenPayment_whenFindByStatus_thenReturnPayments() throws Exception {
    OrderEntity orderEntity = new OrderEntity("1234");
    List<OrderEntity> allOrderEntities = Arrays.asList(orderEntity);

    given(orderService.findByStatus(Mockito.any())).willReturn(allOrderEntities);

    MvcResult result = mvc.perform(get("/payment")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    assertThat(result.getResponse().getContentAsString()).isNotBlank().isNotNull().isNotEmpty();
  }

  @Test
  public void givenPay_whenMakeAPayment_thenReturnJsonArray() throws Exception {
    OrderEntity orderEntity = new OrderEntity("1234");
    List<OrderEntity> allOrderEntities = Arrays.asList(orderEntity);

    given(orderService.findAllOrders()).willReturn(allOrderEntities);

    jsonPath("$[0].orderNumber", is(orderEntity.getOrderNumber()));
    MvcResult result = mvc.perform(post("/pay")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk()).andReturn();

    assertThat(result.getResponse().getContentAsString()).isNotBlank().isNotNull().isNotEmpty();
  }


}
