package com.order.tracking.controllers;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.order.tracking.config.JmsConfig;
import com.order.tracking.config.TestConfig;
import com.order.tracking.controller.RestOrderController;
import com.order.tracking.entity.Sku.Sku;
import com.order.tracking.entity.order.OrderEntity;
import com.order.tracking.model.order.OrderTracking;
import com.order.tracking.model.order.Payment;
import com.order.tracking.repository.OrderRepository;
import com.order.tracking.services.order.OrderService;
import com.order.tracking.services.queue.QueueService;
import javax.jms.ConnectionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(RestOrderController.class)
@Import({JmsConfig.class, TestConfig.class})
@TestPropertySource(locations = "classpath:test.yaml")
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@AutoConfigureMockMvc
public class RestOrderControllerTest {

  @Autowired
  private MockMvc mvc;

  @MockBean
  private OrderService orderService;

  @MockBean
  private QueueService queueService;

  @MockBean
  private OrderRepository orderRepository;

  @Value("${activemq.brokerUrl}")
  private String brokerUrl;

  @MockBean
  @Qualifier("jmsConnectionFactory")
  ConnectionFactory jmsConnectionFactory;


  @Before
  public void setUp() {

    OrderEntity orderEntity = new OrderEntity("12234");

    Mockito.when(orderRepository.findByOrderNumber(orderEntity.getOrderNumber()))
        .thenReturn(orderEntity);
  }

  @Test
  public void givenRestOrder_whenSave_thenReturnJsonResponse() throws Exception {

    OrderEntity entity = new OrderEntity("1234");
    Sku sku = new Sku(1, 2);
    entity.getOrders().add(sku);

    given(orderService.save(entity)).willReturn(entity);

    ObjectMapper mapper = new ObjectMapper();
    String input = mapper.writeValueAsString(entity);

    var response = mvc.perform(post("/api/rest/order")
        .contentType(MediaType.APPLICATION_JSON)
        .content(input))
        .andExpect(status().isOk())
        .andReturn().getResponse();

    assertThat(response.getContentAsString()).isNotEmpty();

  }

  @Test
  public void givenRestOrder_whenFindByOrderNumber_thenReturnJsonResponse() throws Exception {

    OrderEntity entity = new OrderEntity("1234");

    given(orderService.findByOrderNumber("1234")).willReturn(entity);
    jsonPath("$.orderNumber", is(entity.getOrderNumber()));
    mvc.perform(get("/api/rest/order/{orderNumber}", entity.getOrderNumber())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.orderNumber", notNullValue()))
        .andExpect(jsonPath("$.orderNumber").value(entity.getOrderNumber()));
  }

  @Test
  public void givenRestPayment_whenMakeAPayment_thenReturnJsonResponse() throws Exception {

    Payment payment = new Payment("123", 20.12);

    given(orderService.processPayment(payment)).willReturn(new OrderEntity());

    ObjectMapper mapper = new ObjectMapper();
    String input = mapper.writeValueAsString(payment);

    var response = mvc.perform(post("/api/rest/payment")
        .contentType(MediaType.APPLICATION_JSON)
        .content(input))
        .andExpect(status().isOk())
        .andReturn().getResponse();

    assertThat(response.getContentAsString()).isNotEmpty();

  }

  @Test
  public void givenRestOrders_whenRetrievePendingAndShippedOrderCount_thenReturnJsonResponse()
      throws Exception {

    OrderTracking orderTracking = new OrderTracking(1, 1);

    given(orderService.retrieveOrderTrackingInformation()).willReturn(orderTracking);

    jsonPath("$.orderShipped", is(orderTracking.getNumberShipped()));
    mvc.perform(get("/api/rest/orders")
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.numberShipped").value(orderTracking.getNumberShipped()))
        .andExpect(jsonPath("$.numberWaitingForPayment")
            .value(orderTracking.getNumberWaitingForPayment()));
  }


}
