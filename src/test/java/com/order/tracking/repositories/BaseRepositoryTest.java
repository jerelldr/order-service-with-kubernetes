package com.order.tracking.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import com.order.tracking.config.JmsConfig;
import com.order.tracking.config.TestConfig;
import com.order.tracking.constants.Status;
import com.order.tracking.entity.Sku.Sku;
import com.order.tracking.entity.order.OrderEntity;
import com.order.tracking.repository.BaseRepository;
import com.order.tracking.services.queue.QueueService;
import javax.jms.ConnectionFactory;
import javax.transaction.Transactional;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestEntityManager
@Import({TestConfig.class, JmsConfig.class})
@Transactional
@Rollback
@TestPropertySource(locations = "classpath:test.yaml")
public class BaseRepositoryTest {

  @Autowired
  private BaseRepository baseRepository;

  @MockBean
  private QueueService queueService;

  @MockBean
  @Qualifier("jmsConnectionFactory")
  ConnectionFactory jmsConnectionFactory;

  @Test
  public void whenSave_AndOrderEntityIsValid_ThenReturnOrderEntity() {

    Sku sku = new Sku();
    OrderEntity orderEntity = new OrderEntity("1234");
    orderEntity.setStatus(Status.PENDING);
    orderEntity.getOrders().add(sku);

    var created = baseRepository.save(orderEntity);

    assertThat(created).isNotNull();

    assertThat(created.getOrderNumber()).isNotEmpty();
    assertThat(created.getStatus()).isEqualTo(orderEntity.getStatus());
  }

  @Test
  public void whenFindByOrderNumber_thenReturnOrder() {

    Sku sku = new Sku();
    sku.setPrice(1);
    OrderEntity orderEntity = new OrderEntity("1234");
    orderEntity.setStatus(Status.PENDING);
    orderEntity.getOrders().add(sku);

    var created = baseRepository.save(orderEntity);

    assertThat((created.getOrderNumber())).isNotEmpty();

    var found = baseRepository.findByOrderNumber(created.getOrderNumber());

    assertThat(found).isNotNull();

    assertThat(found.getOrderNumber()).isNotEmpty();
    assertThat(found.getStatus()).isEqualTo(created.getStatus());
  }


  @Test
  public void whenFindAll_thenReturnAllOrders() {

    Sku sku = new Sku();
    sku.setPrice(1);
    OrderEntity orderEntity = new OrderEntity("1234");
    orderEntity.setStatus(Status.PENDING);
    orderEntity.getOrders().add(sku);

    var created = baseRepository.save(orderEntity);

    assertThat((created.getOrderNumber())).isNotEmpty();

    var found = baseRepository.findAll();

    assertThat(found).isNotNull();
    assertThat(found).hasSize(1);
  }

  //@Test
  public void whenFindByOrderNumber_thenReturnOrderByOrderNumber() {

    Sku sku = new Sku();
    sku.setPrice(1);
    OrderEntity orderEntity = new OrderEntity("1234");
    orderEntity.getOrders().add(sku);
    orderEntity.setStatus(Status.PENDING);

    orderEntity.getOrders().add(sku);

    var created = baseRepository.save(orderEntity);

    assertThat((created.getOrderNumber())).isNotEmpty();

    baseRepository.updateStatus(created.getOrderNumber(),Status.SHIPPED);

    var found = baseRepository.findByOrderNumber(created.getOrderNumber());

    assertThat(found).isNotNull();

    assertThat(found.getStatus()).isEqualTo(Status.SHIPPED);
  }

  @Test
  public void whenFindByStatus_thenReturnOrderWithShippedStatus() {

    Sku sku = new Sku();
    sku.setPrice(1);
    OrderEntity orderEntity = new OrderEntity("1234");
    orderEntity.setStatus(Status.PENDING);

    orderEntity.getOrders().add(sku);

    var created = baseRepository.save(orderEntity);

    assertThat((created.getOrderNumber())).isNotEmpty();

    var found = baseRepository.findByStatus(Status.PENDING);

    assertThat(found).isNotNull();

    assertThat(found).isNotNull();
    assertThat(((OrderEntity)found.get(0)).getStatus()).isEqualTo(orderEntity.getStatus());
  }

}
