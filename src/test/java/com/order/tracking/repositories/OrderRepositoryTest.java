package com.order.tracking.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import com.order.tracking.config.JmsConfig;
import com.order.tracking.config.TestConfig;
import com.order.tracking.entity.order.OrderEntity;
import com.order.tracking.entity.Sku.Sku;
import com.order.tracking.repository.OrderRepository;
import com.order.tracking.services.queue.QueueService;
import javax.jms.ConnectionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestEntityManager
@Import({TestConfig.class, JmsConfig.class})
@TestPropertySource(locations = "classpath:test.yaml")
public class OrderRepositoryTest {

  @Autowired
  private OrderRepository repository;

  @MockBean
  private QueueService queueService;

  @MockBean
  @Qualifier("jmsConnectionFactory")
  ConnectionFactory jmsConnectionFactory;

  @Test
  public void whenFindByOrderNumber_thenReturnOrder() {
    Sku sku = new Sku();
    OrderEntity orderEntity = new OrderEntity("12234");

    orderEntity.getOrders().add(sku);

    var created = repository.save(orderEntity);

    OrderEntity found = repository
        .findByOrderNumber(created.getOrderNumber());

    assertThat(found).isNotNull();

    assertThat(found.getOrderNumber()).isEqualTo(created.getOrderNumber());
  }

}
