package services.order;

import static org.assertj.core.api.Assertions.assertThat;

import com.order.tracking.entity.order.OrderEntity;
import com.order.tracking.repository.OrderRepository;
import com.order.tracking.services.mappers.OrderMapper;
import com.order.tracking.services.order.OrderService;
import com.order.tracking.services.queue.QueueService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class OrderServiceTest {

  @TestConfiguration
  static class OrderServiceTestContextConfiguration {

    @Bean
    public OrderService orderService() {
      return new OrderService();
    }
  }

  @Autowired
  private OrderService orderService;

  @MockBean
  private OrderRepository orderRepository;

  @MockBean
  private QueueService queueService;

  @MockBean
  private OrderMapper orderMapper;

  @Before
  public void setUp() {

    OrderEntity orderEntity = new OrderEntity("1234");

    Mockito.when(orderRepository.findByOrderNumber(orderEntity.getOrderNumber()))
        .thenReturn(orderEntity);
  }

  @Test
  public void whenValidOrderNumber_thenOrderShouldBeFound() {
    String orderNumber = "1234";
    OrderEntity found = orderService.findByOrderNumber(orderNumber);

    assertThat(found).isNotNull();
    assertThat(found.getOrderNumber()).isEqualTo(orderNumber);
  }



}
