package services.queue;

import com.order.tracking.model.jms.JmsOrderMessage;
import com.order.tracking.services.order.OrderService;
import com.order.tracking.services.queue.QueueService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class QueueServiceTest {

  @TestConfiguration
  static class OrderServiceTestContextConfiguration {

    @Bean
    public QueueService queueService() {
      return new QueueService();
    }
  }

  @Autowired
  private QueueService queueService;

  @MockBean
  JmsTemplate jmsTemplate;

  @MockBean
  OrderService orderService;

  @Test
  public void whenValidMessage_thenMessageShouldBeConsumed() {
    var orderMessage = new JmsOrderMessage();
      queueService.send("queue", orderMessage);

    }
  }

